use std::collections::BTreeMap;
use identity::*;

pub mod identity;
pub mod message;

//TODO: make it statically checked
type Path = &'static str;
//TODO: make it statically checked
type Subtype = &'static str;
//TODO: make it statically checked
type Parameters = BTreeMap<&'static str, &'static str>; 

pub enum Version {
    Http10,
    Http11,
    Http20,
}

/// Target of an HTTP request.
/// [Definition](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Identifying_resources_on_the_Web).
#[derive(Clone, Debug)]
pub struct Resource {
    uri: URI,
}

/// The user-agent is any tool that acts on the behalf of the user.
pub struct Client{}

/// Serves the document as requested by the client.
pub struct Server {
}

/// 
///  Proxies may perform numerous functions:
///  * caching (the cache can be public or private, like the browser cache)
///  * filtering (like an antivirus scan or parental controls)
///  * load balancing (to allow multiple servers to serve the different requests)
///  * authentication (to control access to different resources)
///  * logging (allowing the storage of historical information)
pub struct ProxyServer {
    origin: Box<Server>
}

/// A media type is a standard that indicates the nature and format
/// of a document, file, or assortment of bytes.
/// [Definition](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types).
//TODO: Make it Copy.
#[derive(Clone, Debug)]
pub enum MimeType {
    /// Any kind of binary data that doesn't fall explicitly into one of the other types.
    Application(Subtype, Parameters),
    /// Audio or music data.  Audio(Subtype, Parameters),
    /// Font/typeface data.
    Font(Subtype, Parameters),
    /// Image or graphical data including both bitmap and vector still images as well as animated
    /// versions of still image formats.
    Image(Subtype, Parameters),
    /// Model data for a 3D object or scene.
    Model(Subtype, Parameters),
    /// Text-only data including any human-readable content, source code, or textual data.
    Text(Subtype, Parameters),
    /// Video data or files.
    Video(Subtype, Parameters),
    /// A message that encapsulates other messages.
    Message(Subtype, Parameters),
    /// Data that is comprised of multiple components which may individually have different MIME
    /// types.
    Multipart(Subtype, Parameters),
}
